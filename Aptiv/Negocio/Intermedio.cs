﻿using System;
using System.Collections.Generic;
using System.Data;
using Datos;
using Entidades;

namespace Negocio
{
    public class Intermedio
    {
        private Consultas ObjConsulta = new Consultas();
        public DataTable Tabla = new DataTable();
        private List<object> ValueForms = new List<object>();

        public Intermedio()
        {
            
        } // Clave de Constructor: Intermedio-C1

        public DataTable LogeoUsuario(string Usuario, string Contrasenia)
        {
            ValueForms.Clear();
            try
            {
                ValueForms.Add(Usuario);
                ValueForms.Add(Contrasenia);
                return ObjConsulta.ConsultaLeerTablas("sp_loguser", ValueForms);
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                ValueForms.Clear();
            }
        } //Clave de Método: Intermedio-LU

        public List<string> LlenarCombo(Combo TipoDeCombo)
        {
            List<string> List = new List<string>();
            string ProcedimientoAlmacenado = "";
            switch ((byte)TipoDeCombo)
            {
                case (byte)Combo.Maquina:
                    {
                        ProcedimientoAlmacenado = "sp_cargar_maquinas";
                        break;
                    }
                case (byte)Combo.Ruta:
                    {
                        ProcedimientoAlmacenado = "sp_cargar_rutas";
                        break;
                    }
            }
            foreach (DataRow Fila in ObjConsulta.ConsultaLeerTablas(ProcedimientoAlmacenado, ValueForms).Rows)
            {
                List.Add(Fila["id"].ToString());
            }
            return List;
        } // Clave de Método: Intermedio-LC

        public bool ExisteId(Existe Existencia, string Texto)
        {
            ValueForms.Clear();
            string ProcedimientoAlmacenado = "";
            try
            {
                switch ((byte)Existencia)
                {
                    case (byte)Existe.Terminal:
                        {
                            ProcedimientoAlmacenado = "sp_existe_terminal";
                            break;
                        }
                    case (byte)Existe.Tara:
                        {
                            ProcedimientoAlmacenado = "sp_existe_tara";
                            break;
                        }
                    case (byte)Existe.Empleado:
                        {
                            ProcedimientoAlmacenado = "sp_existe_empleado";
                            break;
                        }
                }
                ValueForms.Add(Texto);
                return Texto == ObjConsulta.ConsultaLeerTablas(ProcedimientoAlmacenado, ValueForms).Rows[0]["id"].ToString();
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                ValueForms.Clear();
            }
        } //Clave de Método: Intermedio-EI
    }
}
