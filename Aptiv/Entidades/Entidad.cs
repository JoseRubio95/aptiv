﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Entidad
    {
        public string IdEmpleado { get; set; }
        public string Usuario { get; set; }
        public string Contrasenia { get; set; }
        public string Nombre { get; set; }
        public int IdPuesto { get; set; }
        public string DescripcionPuesto { get; set; }
    }

    public class PesarTerminal
    {
        public string Terminal { get; set; }
        public string Tara { get; set; }
        public string IdEmpleado { get; set; }
        public string IdMaquina { get; set; }
        public int IdRuta { get; set; }
        public double Peso { get; set; }
        public double Cantidad { get; set; }
    }

    public enum Logeo
    {
        Iniciar = 1,
        Cerrar = 2
    }

    public enum Resultado
    {
        Correcto = 1,
        Incorrecto = 2,
        Advertencia = 3
    }

    public enum Combo
    {
        Maquina = 1,
        Ruta = 2
    }

    public enum Existe
    {
        Nada = 0,
        Terminal = 1,
        Tara = 2,
        Empleado = 3
    }
}
