﻿namespace Presentacion
{
    partial class Principal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.PanelContenido = new System.Windows.Forms.Panel();
            this.MenuPrincipal = new System.Windows.Forms.MenuStrip();
            this.InicioMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.PesarMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.DevolverTerminalMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.LocalizarMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.MovimientosSubMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.InventarioSubMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ScrapSubMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.InventarioMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ConfiguracionMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.EmpleadoSubMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.TaraSubMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.UnidadDeMedidaSubMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.CerrarSesionMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.PanelMenu.SuspendLayout();
            this.MenuPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelMenu
            // 
            this.PanelMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelMenu.AutoScroll = true;
            this.PanelMenu.BackColor = System.Drawing.Color.Transparent;
            this.PanelMenu.Controls.Add(this.PanelContenido);
            this.PanelMenu.Controls.Add(this.MenuPrincipal);
            this.PanelMenu.Location = new System.Drawing.Point(3, 65);
            this.PanelMenu.Margin = new System.Windows.Forms.Padding(0);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(1435, 835);
            this.PanelMenu.TabIndex = 0;
            // 
            // PanelContenido
            // 
            this.PanelContenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContenido.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.PanelContenido.Location = new System.Drawing.Point(0, 33);
            this.PanelContenido.Margin = new System.Windows.Forms.Padding(0);
            this.PanelContenido.Name = "PanelContenido";
            this.PanelContenido.Size = new System.Drawing.Size(1435, 802);
            this.PanelContenido.TabIndex = 3;
            // 
            // MenuPrincipal
            // 
            this.MenuPrincipal.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InicioMenu,
            this.PesarMenu,
            this.DevolverTerminalMenu,
            this.LocalizarMenu,
            this.ReportesMenu,
            this.InventarioMenu,
            this.ConfiguracionMenu,
            this.CerrarSesionMenu});
            this.MenuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.MenuPrincipal.Name = "MenuPrincipal";
            this.MenuPrincipal.Size = new System.Drawing.Size(1435, 33);
            this.MenuPrincipal.TabIndex = 2;
            this.MenuPrincipal.Text = "menuStrip1";
            // 
            // InicioMenu
            // 
            this.InicioMenu.Name = "InicioMenu";
            this.InicioMenu.Size = new System.Drawing.Size(70, 29);
            this.InicioMenu.Text = "Inicio";
            this.InicioMenu.Click += new System.EventHandler(this.ClickMethodMenu);
            // 
            // PesarMenu
            // 
            this.PesarMenu.Name = "PesarMenu";
            this.PesarMenu.Size = new System.Drawing.Size(69, 29);
            this.PesarMenu.Text = "Pesar";
            this.PesarMenu.Click += new System.EventHandler(this.ClickMethodMenu);
            // 
            // DevolverTerminalMenu
            // 
            this.DevolverTerminalMenu.Name = "DevolverTerminalMenu";
            this.DevolverTerminalMenu.Size = new System.Drawing.Size(98, 29);
            this.DevolverTerminalMenu.Text = "Devolver";
            // 
            // LocalizarMenu
            // 
            this.LocalizarMenu.Name = "LocalizarMenu";
            this.LocalizarMenu.Size = new System.Drawing.Size(99, 29);
            this.LocalizarMenu.Text = "Localizar";
            this.LocalizarMenu.Click += new System.EventHandler(this.ClickMethodMenu);
            // 
            // ReportesMenu
            // 
            this.ReportesMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MovimientosSubMenu,
            this.InventarioSubMenu,
            this.ScrapSubMenu});
            this.ReportesMenu.Name = "ReportesMenu";
            this.ReportesMenu.Size = new System.Drawing.Size(97, 29);
            this.ReportesMenu.Text = "Reportes";
            // 
            // MovimientosSubMenu
            // 
            this.MovimientosSubMenu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.MovimientosSubMenu.Name = "MovimientosSubMenu";
            this.MovimientosSubMenu.Size = new System.Drawing.Size(173, 26);
            this.MovimientosSubMenu.Text = "Movimientos";
            // 
            // InventarioSubMenu
            // 
            this.InventarioSubMenu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.InventarioSubMenu.Name = "InventarioSubMenu";
            this.InventarioSubMenu.Size = new System.Drawing.Size(173, 26);
            this.InventarioSubMenu.Text = "Inventario";
            // 
            // ScrapSubMenu
            // 
            this.ScrapSubMenu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.ScrapSubMenu.Name = "ScrapSubMenu";
            this.ScrapSubMenu.Size = new System.Drawing.Size(173, 26);
            this.ScrapSubMenu.Text = "Desperdicio";
            // 
            // InventarioMenu
            // 
            this.InventarioMenu.Name = "InventarioMenu";
            this.InventarioMenu.Size = new System.Drawing.Size(109, 29);
            this.InventarioMenu.Text = "Inventario";
            // 
            // ConfiguracionMenu
            // 
            this.ConfiguracionMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EmpleadoSubMenu,
            this.TaraSubMenu,
            this.UnidadDeMedidaSubMenu});
            this.ConfiguracionMenu.Name = "ConfiguracionMenu";
            this.ConfiguracionMenu.Size = new System.Drawing.Size(144, 29);
            this.ConfiguracionMenu.Text = "Configuración";
            // 
            // EmpleadoSubMenu
            // 
            this.EmpleadoSubMenu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.EmpleadoSubMenu.Name = "EmpleadoSubMenu";
            this.EmpleadoSubMenu.Size = new System.Drawing.Size(209, 26);
            this.EmpleadoSubMenu.Text = "Empleado";
            // 
            // TaraSubMenu
            // 
            this.TaraSubMenu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.TaraSubMenu.Name = "TaraSubMenu";
            this.TaraSubMenu.Size = new System.Drawing.Size(209, 26);
            this.TaraSubMenu.Text = "Tara";
            // 
            // UnidadDeMedidaSubMenu
            // 
            this.UnidadDeMedidaSubMenu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.UnidadDeMedidaSubMenu.Name = "UnidadDeMedidaSubMenu";
            this.UnidadDeMedidaSubMenu.Size = new System.Drawing.Size(209, 26);
            this.UnidadDeMedidaSubMenu.Text = "Unidad de Medida";
            // 
            // CerrarSesionMenu
            // 
            this.CerrarSesionMenu.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.CerrarSesionMenu.Name = "CerrarSesionMenu";
            this.CerrarSesionMenu.Size = new System.Drawing.Size(137, 29);
            this.CerrarSesionMenu.Text = "Cerrar Sesión";
            this.CerrarSesionMenu.Click += new System.EventHandler(this.ClickMethodMenu);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1440, 900);
            this.Controls.Add(this.PanelMenu);
            this.MainMenuStrip = this.MenuPrincipal;
            this.MinimumSize = new System.Drawing.Size(1440, 900);
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "APTIV - Sistema de Inventario de Terminales";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClosingMetodo);
            this.PanelMenu.ResumeLayout(false);
            this.PanelMenu.PerformLayout();
            this.MenuPrincipal.ResumeLayout(false);
            this.MenuPrincipal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelMenu;
        private System.Windows.Forms.MenuStrip MenuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem PesarMenu;
        private System.Windows.Forms.ToolStripMenuItem LocalizarMenu;
        private System.Windows.Forms.ToolStripMenuItem ConfiguracionMenu;
        private System.Windows.Forms.ToolStripMenuItem TaraSubMenu;
        private System.Windows.Forms.ToolStripMenuItem EmpleadoSubMenu;
        private System.Windows.Forms.ToolStripMenuItem UnidadDeMedidaSubMenu;
        private System.Windows.Forms.ToolStripMenuItem ReportesMenu;
        private System.Windows.Forms.ToolStripMenuItem CerrarSesionMenu;
        private System.Windows.Forms.Panel PanelContenido;
        private System.Windows.Forms.ToolStripMenuItem MovimientosSubMenu;
        private System.Windows.Forms.ToolStripMenuItem InventarioSubMenu;
        private System.Windows.Forms.ToolStripMenuItem ScrapSubMenu;
        private System.Windows.Forms.ToolStripMenuItem InicioMenu;
        private System.Windows.Forms.ToolStripMenuItem DevolverTerminalMenu;
        private System.Windows.Forms.ToolStripMenuItem InventarioMenu;
    }
}

