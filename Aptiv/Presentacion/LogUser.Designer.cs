﻿namespace Presentacion
{
    partial class LogUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogUser));
            this.TextBoxUser = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.TextBoxPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.ButtonAceptar = new System.Windows.Forms.Button();
            this.ErrorLogUser = new System.Windows.Forms.ErrorProvider(this.components);
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorLogUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxUser
            // 
            this.TextBoxUser.Depth = 0;
            this.TextBoxUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxUser.Hint = "";
            this.TextBoxUser.Location = new System.Drawing.Point(76, 325);
            this.TextBoxUser.MaxLength = 50;
            this.TextBoxUser.MouseState = MaterialSkin.MouseState.HOVER;
            this.TextBoxUser.Name = "TextBoxUser";
            this.TextBoxUser.PasswordChar = '\0';
            this.TextBoxUser.SelectedText = "";
            this.TextBoxUser.SelectionLength = 0;
            this.TextBoxUser.SelectionStart = 0;
            this.TextBoxUser.Size = new System.Drawing.Size(317, 23);
            this.TextBoxUser.TabIndex = 0;
            this.TextBoxUser.TabStop = false;
            this.TextBoxUser.UseSystemPasswordChar = false;
            this.TextBoxUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxUserKeyPressMetodo);
            // 
            // TextBoxPassword
            // 
            this.TextBoxPassword.Depth = 0;
            this.TextBoxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxPassword.Hint = "";
            this.TextBoxPassword.Location = new System.Drawing.Point(76, 406);
            this.TextBoxPassword.MaxLength = 32;
            this.TextBoxPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.TextBoxPassword.Name = "TextBoxPassword";
            this.TextBoxPassword.PasswordChar = '*';
            this.TextBoxPassword.SelectedText = "";
            this.TextBoxPassword.SelectionLength = 0;
            this.TextBoxPassword.SelectionStart = 0;
            this.TextBoxPassword.Size = new System.Drawing.Size(317, 23);
            this.TextBoxPassword.TabIndex = 1;
            this.TextBoxPassword.TabStop = false;
            this.TextBoxPassword.UseSystemPasswordChar = false;
            this.TextBoxPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBotPasswordKeyPressMetodo);
            // 
            // ButtonAceptar
            // 
            this.ButtonAceptar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(124)))), ((int)(((byte)(0)))));
            this.ButtonAceptar.FlatAppearance.BorderSize = 0;
            this.ButtonAceptar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(108)))), ((int)(((byte)(0)))));
            this.ButtonAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAceptar.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAceptar.ForeColor = System.Drawing.Color.White;
            this.ButtonAceptar.Location = new System.Drawing.Point(38, 473);
            this.ButtonAceptar.Name = "ButtonAceptar";
            this.ButtonAceptar.Size = new System.Drawing.Size(355, 47);
            this.ButtonAceptar.TabIndex = 2;
            this.ButtonAceptar.Text = "Iniciar Sesión";
            this.ButtonAceptar.UseVisualStyleBackColor = false;
            this.ButtonAceptar.Click += new System.EventHandler(this.ButtonAceptar_Click);
            // 
            // ErrorLogUser
            // 
            this.ErrorLogUser.BlinkRate = 200;
            this.ErrorLogUser.ContainerControl = this;
            this.ErrorLogUser.Icon = ((System.Drawing.Icon)(resources.GetObject("ErrorLogUser.Icon")));
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::Presentacion.Properties.Resources.Lock;
            this.pictureBox3.Location = new System.Drawing.Point(38, 397);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(32, 32);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::Presentacion.Properties.Resources.User;
            this.pictureBox2.Location = new System.Drawing.Point(38, 316);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Presentacion.Properties.Resources.Logo;
            this.pictureBox1.Location = new System.Drawing.Point(115, 81);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // LogUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 550);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ButtonAceptar);
            this.Controls.Add(this.TextBoxPassword);
            this.Controls.Add(this.TextBoxUser);
            this.MaximumSize = new System.Drawing.Size(430, 550);
            this.MinimumSize = new System.Drawing.Size(430, 550);
            this.Name = "LogUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "APTIV- Inicio de Sesión";
            ((System.ComponentModel.ISupportInitialize)(this.ErrorLogUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField TextBoxUser;
        private MaterialSkin.Controls.MaterialSingleLineTextField TextBoxPassword;
        private System.Windows.Forms.Button ButtonAceptar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ErrorProvider ErrorLogUser;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}