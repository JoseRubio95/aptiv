﻿namespace Presentacion
{
    partial class Localizar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxPeso = new System.Windows.Forms.TextBox();
            this.ButtonRegistrar = new System.Windows.Forms.Button();
            this.GridLocalizar = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLocalizar)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label6.Location = new System.Drawing.Point(511, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 25);
            this.label6.TabIndex = 13;
            this.label6.Text = "Esperando lectura...";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.label1.Location = new System.Drawing.Point(176, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(275, 30);
            this.label1.TabIndex = 11;
            this.label1.Text = "Esperando terminal a leer...";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Presentacion.Properties.Resources.Unchecked;
            this.pictureBox1.Location = new System.Drawing.Point(457, 110);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.label2.Location = new System.Drawing.Point(106, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(206, 30);
            this.label2.TabIndex = 14;
            this.label2.Text = "O puedes escribirlo:";
            // 
            // TextBoxPeso
            // 
            this.TextBoxPeso.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TextBoxPeso.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.TextBoxPeso.Location = new System.Drawing.Point(318, 204);
            this.TextBoxPeso.Name = "TextBoxPeso";
            this.TextBoxPeso.Size = new System.Drawing.Size(318, 32);
            this.TextBoxPeso.TabIndex = 19;
            // 
            // ButtonRegistrar
            // 
            this.ButtonRegistrar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ButtonRegistrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(124)))), ((int)(((byte)(0)))));
            this.ButtonRegistrar.FlatAppearance.BorderSize = 0;
            this.ButtonRegistrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(108)))), ((int)(((byte)(0)))));
            this.ButtonRegistrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonRegistrar.Font = new System.Drawing.Font("MS UI Gothic", 18F);
            this.ButtonRegistrar.ForeColor = System.Drawing.Color.White;
            this.ButtonRegistrar.Location = new System.Drawing.Point(654, 204);
            this.ButtonRegistrar.Name = "ButtonRegistrar";
            this.ButtonRegistrar.Size = new System.Drawing.Size(171, 32);
            this.ButtonRegistrar.TabIndex = 20;
            this.ButtonRegistrar.Text = "LOCALIZAR";
            this.ButtonRegistrar.UseVisualStyleBackColor = false;
            // 
            // GridLocalizar
            // 
            this.GridLocalizar.BackgroundColor = System.Drawing.Color.White;
            this.GridLocalizar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridLocalizar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GridLocalizar.Location = new System.Drawing.Point(0, 281);
            this.GridLocalizar.Name = "GridLocalizar";
            this.GridLocalizar.Size = new System.Drawing.Size(930, 219);
            this.GridLocalizar.TabIndex = 21;
            // 
            // Localizar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 500);
            this.Controls.Add(this.GridLocalizar);
            this.Controls.Add(this.ButtonRegistrar);
            this.Controls.Add(this.TextBoxPeso);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(930, 500);
            this.MinimumSize = new System.Drawing.Size(930, 500);
            this.Name = "Localizar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "APTIV - Localizar Terminal";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLocalizar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxPeso;
        private System.Windows.Forms.Button ButtonRegistrar;
        private System.Windows.Forms.DataGridView GridLocalizar;
    }
}