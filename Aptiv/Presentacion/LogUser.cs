﻿using System;
using System.Data;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using Entidades;
using Negocio;

namespace Presentacion
{
    public partial class LogUser : MaterialForm
    {
        private Entidad ObjEntidad = new Entidad();
        private Intermedio ObjIntermedio = new Intermedio();
        private Principal ObjPrincipal;
        private DataTable Tabla = new DataTable();
        private Control[] ItemsControl;
        private readonly byte Opcion;
        private byte Index;

        public LogUser()
        {
            InitializeComponent();
            MaterialSkinManager SkinManager = MaterialSkinManager.Instance;
            SkinManager.AddFormToManage(this);
            SkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            SkinManager.ColorScheme = new ColorScheme(Primary.Orange800, Primary.Orange700, Primary.Orange600, Accent.Blue700, TextShade.BLACK);

            TextBoxPassword.PasswordChar = '*';
            if (Opcion.Equals(0))
            {
                Opcion = (byte)Logeo.Iniciar;
                this.Text = "Iniciar Sesión";
            }
            else
            {
                this.Text = "Cerrar Sesión";
            }
        } //Clave de Constructor: LogUser-C1

        public LogUser(Entidad ObjEntidad, Logeo Opcion)
        {
            InitializeComponent();
            this.ObjEntidad = ObjEntidad;
            this.Opcion = (byte)Opcion;
            TextBoxPassword.PasswordChar = '*';
            this.Text = Opcion.Equals(Logeo.Iniciar) ? "APTIV - Iniciar Sesión" : "APTIV - Cerrar Sesión";
            TextBoxUser.Focus();
        } //Clave de Constructor: LogUser-C2

        private void ButtonAceptar_Click(object sender, EventArgs e)
        {
            ErrorLogUser.Clear();
            if (TextBoxUser.Text == "")
            {
                ErrorLogUser.SetError(TextBoxUser, "Ingrese un usuario");
            }
            else if (TextBoxPassword.Text == "")
            {
                ErrorLogUser.SetError(TextBoxPassword, "Ingrese una contraseña");
            }
            switch (Opcion)
            {
                case (byte)Logeo.Iniciar:
                    {
                        Tabla = ObjIntermedio.LogeoUsuario(TextBoxUser.Text.Trim(), TextBoxPassword.Text.Trim());
                        if (Tabla != null)
                        {
                            if (Tabla.Rows.Count > 0)
                            {
                                ObjEntidad.IdEmpleado = Tabla.Rows[0]["idempleado"].ToString();
                                ObjEntidad.Usuario = Tabla.Rows[0]["usuario"].ToString();
                                ObjEntidad.Contrasenia = Tabla.Rows[0]["contra"].ToString();
                                ObjEntidad.Nombre = Tabla.Rows[0]["nombre"].ToString();
                                ObjEntidad.IdPuesto = (int)Tabla.Rows[0]["idpuesto"];
                                ObjEntidad.DescripcionPuesto = Tabla.Rows[0]["descripcion"].ToString();
                                if (TextBoxUser.Text == ObjEntidad.IdEmpleado)
                                {
                                    Hide();
                                    ObjPrincipal = new Principal(ObjEntidad);
                                    ObjPrincipal.Show();
                                }
                            }
                            else
                            {
                                ErrorLogUser.SetError(TextBoxUser, "No existe el usuario");
                            }
                        }
                        else
                        {
                            ErrorLogUser.SetError(TextBoxUser, "No existe el usuario");
                        }
                        break;
                    }
                case (byte)Logeo.Cerrar:
                    {
                        if (TextBoxUser.Text.Equals(ObjEntidad.IdEmpleado) && TextBoxPassword.Text.Equals(ObjEntidad.Contrasenia))
                        {
                            Application.Exit();
                        }
                        else
                        {
                            ErrorLogUser.SetError(TextBoxUser, "El usuario no coincide con el del inicio de sesión");
                        }
                        break;
                    }
                default:
                    throw new Exception("Exception: LogUser-BA_C", new IndexOutOfRangeException());
            }
        }

        private void TextBoxUserKeyPressMetodo(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                TextBoxPassword.Focus();
            }
        }

        private void TextBotPasswordKeyPressMetodo(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                ButtonAceptar.PerformClick();
            }
        }
    }
}
