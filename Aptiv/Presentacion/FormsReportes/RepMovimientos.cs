﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Presentacion.FormsReportes
{
    public partial class RepMovimientos : MaterialForm
    {
        public RepMovimientos()
        {
            InitializeComponent();
            MaterialSkinManager SkinManager = MaterialSkinManager.Instance;
            SkinManager.AddFormToManage(this);
            SkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            SkinManager.ColorScheme = new ColorScheme(Primary.Orange800, Primary.Orange700, Primary.Orange600, Accent.Blue700, TextShade.BLACK);
        }
    }
}
