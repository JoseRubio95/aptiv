﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Presentacion
{
    public partial class Inicio : Form
    {
        private PictureBox[] Links;
        private byte Index;

        public Inicio()
        {
            InitializeComponent();
            Links = new PictureBox[] { PictureBoxGitLab, PictureBoxFacebook };
        }

        private void LinksClickMethod(object sender, EventArgs e)
        {
            for (; Index < Links.Length; Index++)
                if (Links[Index] == sender)
                    break;
            switch (Index)
            {
                case 0: //GitLab
                    {
                        Process.Start("https://about.gitlab.com/");
                        break;
                    }
                case 1: //Facebook
                    {
                        Process.Start("https://www.facebook.com/acevi.delphi");
                        break;
                    }
                default:
                    throw new Exception("Excepción en Método: Inicio-LCM", new IndexOutOfRangeException());
            }
            Index = 0;
        } //Clave de Método: Inicio-LCM
    }
}
