﻿using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using System.Data.SqlClient;
using System.Configuration;

namespace Datos
{
    public class Consultas
    {
        /// <summary>
        /// Metódo de consulta para leer filas de tablas en la base de datos.
        /// </summary>
        /// <param name="ProcedimientoAlmacenado">Nombre del procedimiento almacenado.</param>
        /// <param name="Valores">Lista de valores que serán los parámetros para el procedimiento almacenado.</param>
        /// <returns></returns>
        public DataTable ConsultaLeerTablas(string ProcedimientoAlmacenado, List<object> Valores)
        {
            DataTable Tabla = new DataTable();
            try
            {
                using (NpgsqlConnection Conexion = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["ConnectionDataBase"].ConnectionString))
                {
                    using (NpgsqlCommand Comando  = Conexion.CreateCommand())
                    {
                        Comando.CommandText = ProcedimientoAlmacenado;
                        Comando.CommandType = CommandType.StoredProcedure;
                        Comando.Parameters.Clear();
                        for (int i = 0; i < Valores.Count; i++) Comando.Parameters.AddWithValue("@p" + (i + 1), Valores[i]);
                        if (Comando.Connection.State == ConnectionState.Closed) Conexion.Open();
                        Comando.Prepare();
                        using (NpgsqlDataReader LeerFilas = Comando.ExecuteReader())
                        {
                            Tabla.Load(LeerFilas);
                        }
                    }
                }
            }
            catch (Exception Error)
            {
                Console.WriteLine(Error.ToString());
            }
            return Tabla;
        } //Consultas-R

        /// <summary>
        /// Método de consulta para insertar, actualizar y eliminar en la base de datos.
        /// </summary>
        /// <param name="ProcedimientoAlmacenado">Nombre del procedimiento almacenado.</param>
        /// <param name="Valores">Lista de valores que serán los parámetros para el procedimiento almacenado.</param>
        /// <returns></returns>
        public int ConsultaCUD(string ProcedimientoAlmacenado, List<object> Valores)
        {
            int Resultado = 0;
            try
            {
                using (NpgsqlConnection Conexion = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["ConnectionDataBase"].ConnectionString))
                {
                    using (NpgsqlCommand Comando = new NpgsqlCommand(ProcedimientoAlmacenado, Conexion) { CommandType = CommandType.StoredProcedure })
                    {
                        Comando.Parameters.Clear();
                        for (int i = 0; i < Valores.Count; i++) Comando.Parameters.Add(new NpgsqlParameter("p" + (i + 1), Valores[i]));
                        Comando.Prepare();
                        if (Comando.Connection.State == ConnectionState.Closed) Conexion.Open();
                        Resultado = Comando.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {

            }
            return Resultado;
        } //Consultas-CUD
    }
}
