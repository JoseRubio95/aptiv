-------------------------------------------------------------------------------------------------------
insert into estados ("id", "descripcion", "abreviado", "hora_registro", "fecha_registro")
values (1, 'ACTIVO', 'A', current_time, current_date);
insert into estados ("id", "descripcion", "abreviado", "hora_registro", "fecha_registro")
values (2, 'INACTIVO', 'I', current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into rutas ("descripcion", "id_estado", "hora_registro", "fecha_registro")
values('ruta 1', 1, current_time, current_date);
insert into rutas ("descripcion", "id_estado", "hora_registro", "fecha_registro")
values('ruta 2', 1, current_time, current_date);
insert into rutas ("descripcion", "id_estado", "hora_registro", "fecha_registro")
values('ruta 3', 1, current_time, current_date);
insert into rutas ("descripcion", "id_estado", "hora_registro", "fecha_registro")
values('ruta 4', 1, current_time, current_date);
insert into rutas ("descripcion", "id_estado", "hora_registro", "fecha_registro")
values('ruta 5', 1, current_time, current_date);
insert into rutas ("descripcion", "id_estado", "hora_registro", "fecha_registro")
values('ruta 6', 1, current_time, current_date);
insert into rutas ("descripcion", "id_estado", "hora_registro", "fecha_registro")
values('ruta 7', 1, current_time, current_date);
insert into rutas ("descripcion", "id_estado", "hora_registro", "fecha_registro")
values('ruta 8', 1, current_time, current_date);
insert into rutas ("descripcion", "id_estado", "hora_registro", "fecha_registro")
values('ruta 9', 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into contenedores ("abreviado", "descripcion", "id_estado", "hora_registro", "fecha_registro")
values ('NR', 'CONTENEDOR NR', 1, current_time, current_date);
insert into contenedores ("abreviado", "descripcion", "id_estado", "hora_registro", "fecha_registro")
values ('MS', 'CONTENEDOR MS', 1, current_time, current_date);
insert into contenedores ("abreviado", "descripcion", "id_estado", "hora_registro", "fecha_registro")
values ('AJ', 'CONTENEDOR AJ', 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into puestos ("descripcion", "id_estado","hora_registro","fecha_registro")
values('Operador', 1, current_time, current_date);
insert into puestos ("descripcion", "id_estado","hora_registro","fecha_registro")
values('Supervisor de línea', 1, current_time, current_date);
insert into puestos ("descripcion", "id_estado","hora_registro","fecha_registro")
values('Gerente de operaciones', 1, current_time, current_date);
insert into puestos ("descripcion", "id_estado","hora_registro","fecha_registro")
values ('Gerente general', 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into medidas ("descripcion", "abreviado", "id_estado", "hora_registro", "fecha_registro")
values ('PIEZA(S)', 'PZA', 1, current_time, current_date);
insert into medidas ("descripcion", "abreviado", "id_estado", "hora_registro", "fecha_registro")
values ('MILIMETRO(S)', 'MM', 1, current_time, current_date);
insert into medidas ("descripcion", "abreviado", "id_estado", "hora_registro", "fecha_registro")
values ('CENTIMETRO(S)', 'CM', 1, current_time, current_date);
insert into medidas ("descripcion", "abreviado", "id_estado", "hora_registro", "fecha_registro")
values ('METRO(S)', 'M', 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into localizacion ("rack", "seccion", "nivel", "posicion", "id_estado", "hora_registro", "fecha_registro")
values('A7',9,7,'3A', 1, current_time, current_date);
insert into localizacion ("rack", "seccion", "nivel", "posicion", "id_estado", "hora_registro", "fecha_registro")
values('A7',9,7,'3B', 1, current_time, current_date);
insert into localizacion ("rack", "seccion", "nivel", "posicion", "id_estado", "hora_registro", "fecha_registro")
values('A7',9,7,'3C', 1, current_time, current_date);
insert into localizacion ("rack", "seccion", "nivel", "posicion", "id_estado", "hora_registro", "fecha_registro")
values('A7',9,7,'3D', 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into maquinas ("id", "descripcion", "id_estado", "hora_registro", "fecha_registro")
values ('A1', 'Máquina de producción', 1, current_time, current_date);
insert into maquinas ("id", "descripcion", "id_estado", "hora_registro", "fecha_registro")
values ('A2', 'Máquina de corte', 1, current_time, current_date);
insert into maquinas ("id", "descripcion", "id_estado", "hora_registro", "fecha_registro")
values ('B3', 'Máquina de ensamble', 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into empleados ("id", "nombre", "id_puesto", "id_estado", "hora_registro", "fecha_registro")
values('02069003077420181118','José Luis Rubio Pérez', 2, 1, current_time, current_date);
insert into empleados ("id", "nombre", "id_puesto", "id_estado", "hora_registro", "fecha_registro")
values('T154   1351333','Jorge Eduardo Gómez Baray', 1, 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into usuarios ("id_empleado", "usuario", "contra", "id_estado", "hora_registro", "fecha_registro")
values ('02069003077420181118', 'Consu', '1234', 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into terminales ("id", "id_medida","id_localizacion","descripcion","peso","peso_unidad", "id_estado", "hora_registro", "fecha_registro")
values('C600000336', 1, 2, 'TERMINAL DE PRUEBAS', 10, 0.0024, 1, current_time, current_date);
insert into terminales ("id", "id_medida","id_localizacion","descripcion","peso","peso_unidad", "id_estado", "hora_registro", "fecha_registro")
values('2105071642406', 4, 3, 'TERMINAL DE PRUEBAS 2', 5, 0.98, 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into taras values ('GQA4SFKDFNSWZ82F', 'T1', 0.578, 1, current_time, current_date);
insert into taras values ('2107107342106', 'T2', 0.563, 1, current_time, current_date);
insert into taras values ('3', 'T3', 0.534, 1, current_time, current_date);
insert into taras values ('4', 'T4', 0.718, 1, current_time, current_date);
insert into taras values ('5', 'T5', 0.525, 1, current_time, current_date);
insert into taras values ('6', 'T6', 0.641, 1, current_time, current_date);
insert into taras values ('7', 'T7', 0.469, 1, current_time, current_date);
insert into taras values ('8', 'T8', 0, 1, current_time, current_date);
insert into taras values ('9', 'T9', 0.45, 1, current_time, current_date);
insert into taras values ('10', 'T10', 0.607, 1, current_time, current_date);
insert into taras values ('11', 'T11', 0.314, 1, current_time, current_date);
insert into taras values ('12', 'T12', 0.465, 1, current_time, current_date);
insert into taras values ('13', 'T13', 0.609, 1, current_time, current_date);
insert into taras values ('14', 'T14', 0.685, 1, current_time, current_date);
insert into taras values ('15', 'T15', 0.556, 1, current_time, current_date);
insert into taras values ('16', 'T16', 0.538, 1, current_time, current_date);
insert into taras values ('17', 'T17', 0.767, 1, current_time, current_date);
insert into taras values ('18', 'T18', 0.887, 1, current_time, current_date);
insert into taras values ('19', 'T19', 0.688, 1, current_time, current_date);
insert into taras values ('20', 'T20', 0.455, 1, current_time, current_date);
insert into taras values ('21', 'T21', 0.269, 1, current_time, current_date);
insert into taras values ('27', 'T27', 0.497, 1, current_time, current_date);
insert into taras values ('CARRETE AZUL', 'CONT5', 1.282, 1, current_time, current_date);
insert into taras values ('CARRETE NEGRO', 'CONT6', 1.461, 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into terminaltara ("id_terminal", "id_tara", "id_contenedor", "peso_total", "cantidad_std", "semana", "status", "calidad", "maximo", "minimo", "id_estado", "hora_registro", "fecha_registro")
values ('C600000336', 'GQA4SFKDFNSWZ82F', 1, 13.98, 28000, 20, 'A', '-', 42, 13, 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
insert into movimientos ("id_empleado", "id_terminal_tara", "id_maquina", "id_ruta", "estimado", "estimadoreal", "id_estado", "hora_registro", "fecha_registro")
values('02069003077420181118', 1, 'A2', 3, 400, 0, 1, current_time, current_date);
-------------------------------------------------------------------------------------------------------
